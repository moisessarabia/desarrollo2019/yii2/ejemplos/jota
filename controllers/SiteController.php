<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Productos;
use app\models\Categoria;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio(){
        return $this->render('index');
    }
    
    public function actionOfertas(){
          $lista = Productos::find()->where("oferta=1")->all();
         return $this->render('ofertas',['producto'=>$lista]);
    }
    public function actionProductos(){
        $lista = Productos::find()->all();
         return $this->render('productos',['producto'=>$lista]);
         
    }
    public function actionCategorias(){
        $dataProvider = new ActiveDataProvider([
        'query' => Categoria::find(),
    ]);
         return $this->render('categorias',['query'=>$dataProvider]);
    }
    public function actionContacto(){
         return $this->render('contacto');
    }
    
    
        
    }

    