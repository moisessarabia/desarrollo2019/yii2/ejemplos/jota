<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'encodeLabels' =>false,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => '<span class="glyphicon glyphicon-home"></span> Home', 'url' => ['/site/inicio']],
            ['label' => '<span class="glyphicon glyphicon-ok"></span> Ofertas', 'url' => ['/site/ofertas']],
            ['label' => '<span class="glyphicon glyphicon-credit-card"></span> Productos', 'url' => ['/site/productos']],
            ['label' => '<span class="glyphicon glyphicon-certificate"></span> Categorias', 'url' => ['/site/categorias']],        
            ['label' => '<span class="glyphicon glyphicon-info-sign"></span> Nosotros', 'url' => ['/site/nosotros'], 'items' => [
            ['label' => 'Donde estamos', 'url' => ['#']],
            ['label' => 'Quienes somos', 'url' => ['#']],
            ['label' => 'Nuestros productos', 'url' => ['#']],
            ['label' => 'Información', 'url' => ['#']],
            ]],
            ['label' => '<span class="glyphicon glyphicon-list-alt"></span> Contactos', 'url' => ['/site/contacto']],
        
    ]]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
