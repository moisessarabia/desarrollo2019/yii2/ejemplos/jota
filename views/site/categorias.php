<div class="container">
    
    <?php
    use yii\grid\GridView;
    use yii\helpers\Html;
    use app\models\Categoria;
    
        echo GridView::widget([
        'dataProvider' => $query,
            'columns'=> [
                'id',
                'nombre',[

            'attribute' => 'img',

            'format' => 'html',

            'label' => 'Imagen',

            'value' => function ($data) {

                return Html::img('@web/imgs/'.$data['foto'],
                 ['width'=>'150px'],
                 ['height'=>'150px']);

            },

        ],
        [
            'attribute' => 'descripcion',
            'format'=>'html',
            'value'=> function ($data){
                return substr($data['descripcion'],0,2);
            }
        ]
        

                
            ]
        ]);
    ?>
</div>