<?php

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title',                                           // atributo title (en texto plano)
        'description:html',                                // atributo description formateado como HTML
        [                                                  // nombre del propietario del modelo
            'label' => 'Owner',
            'value' => $model->owner->name,
            'contentOptions' => ['class' => 'bg-red'],     // atributos HTML para personalizar el valor
            'captionOptions' => ['tooltip' => 'Tooltip'],  // atributos HTML para personalizar la etiqueta
        ],
                      
    ],
]);