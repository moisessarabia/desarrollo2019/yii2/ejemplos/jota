<?php

/* @var $this yii\web\View */

$this->title = 'La jota';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Jota</h1>
        <h2>La tienda de la maquinaria pesada</h2>
    </div>
    <div class="body-content text-center">
          <?php
            use yii\helpers\Html;
            echo Html::img("@web/imgs/f1.jpg",[
            "class"=>"img-responsive center-block img-rounded",
            "alt"=>"Foto1",
            ]);
          ?>
    </div>
    <br>
    <br>
    <div class="container">
        <p>La Jota es una tienda dedicada a la venta de maquinaria pesada para obra civil,agricultura,ganadería...
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore 
            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse 
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
            qui officia deserunt mollit anim id est laborum.
        </p>
    </div>

 
</div>
