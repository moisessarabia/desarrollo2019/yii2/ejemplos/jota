
<?php
    use yii\helpers\Html;
?>
<h1>Productos</h1>
<div class="container">
    <div class="row">
        <?php
            foreach ($producto as $value){
        ?>
         <div class="productos col-sm-6 bg-primary ">
             <h3>id</h3>
            <?= $value -> id;?>
             <h3>nombre</h3>
            <?= $value -> nombre; ?> 
            <?= Html::img("@web/imgs/" . $value->foto,[
                'class' => 'fpro',
                'width' => '100%',
                'height'=> '300px',
            ] )?>
             <h3>descripcion</h3>
            <?= $value -> descripcion; ?>  
             <h3>precio</h3>
            <?= $value -> precio; ?> 
        </div>
        
        <?php
            }
        ?>
    </div>

</div>