﻿CREATE DATABASE IF NOT EXISTS jota;
  USE jota;
CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  foto text,
  descripcion varchar(100),
  precio int,
  oferta boolean
);
CREATE OR REPLACE TABLE categoria(
  id int PRIMARY KEY,
  nombre varchar(50),
  descripcion varchar(100),
  foto varchar(20)
);

/*CREATE OR REPLACE TABLE pertenecen(
    id int AUTO_INCREMENT PRIMARY KEY,
    producto int UNIQUE,
    categoria int UNIQUE,
    CONSTRAINT fkproducto FOREIGN KEY(producto) REFERENCES productos(id),
    CONSTRAINT fkcategoria FOREIGN KEY(categoria) REFERENCES categoria(id)
  );*/



INSERT INTO productos (id, nombre, foto, descripcion, precio, oferta) values
(1, 'tractor', '1.jpg', 'el tractor sirve para trabajar los campos', 150000, TRUE),
(2, 'cosechadora', '2.jpg', 'la cosechadora sirve para recoger el grano', 420000, FALSE),
(3, 'camion', '3.jpg', 'el camion sirve para transportar objetos', 190000, TRUE),
(4, 'todoterreno', '4.jpg', 'El todoterreno sirve para atravesar lugares complicados', 50000, TRUE),
(5, 'cargadora', '5.jpg', 'la cargadora sirve para cargar productos a granel', 120000, true);

INSERT INTO categoria (id, nombre, descripcion, foto)VALUES 
  (1, 'agricultura', 'en esta categoria se incluye maquinaria para el campo', '6.jpg'),
  (2, 'ganaderia', 'en esta categoria se incluye maquinaria para el ganado', '7.jpg'),
  (3, 'obra civil','en esta categoria se incluye maquinaria para la construcción','8.jpg');





